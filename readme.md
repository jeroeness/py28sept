# Python opdracht 28 september

Notes -- Jeroen Esseveld

Here are some developer notes.

 - The scripts assumes a `<p>` element is never a (recursive) childnode of an other `<p>` element
 - ~~Assuming `<h1>` up to `<h6>` are desired in the output as separate paragraphs~~
 - The script does not filter out image captions
 - iframes are ignored
 - the script recursively searches for p elements
 - I chose pyquery as html parser, this package is embedded in the python environment
 
 ## running
 
In a bash terminal:
 ```bash
 source venv/bin/activate
 python3 main.py
  ```
 ## Example output

```
Warren Buffett / Photo credit: freeimage4life

Veteran investor Warren Buffett considers Paytm stake (India). Buffett’s holding company Berkshire Hathaway is said to have been negotiating a stake in the digital payments and ecommerce giant since February. The investment in Paytm’s parent One97 Communications could reach as much as US$350 million.

It is claimed that the deal would represent the first investment in an Indian company by the “Oracle of Omaha.” Buffett – known for his dedication to long-term investment strategies and modest lifestyle in spite of his massive wealth – launched Berkshire India in partnership with Bajaj Allianz in 2011 to sell insurance products in the country, but exited two years later – supposedly due to “excessive regulation.” (Livemint)

Authorities promise crackdown on transportation sector amid Didi murder controversy (China). The country’s National Development and Reform Commission said that government departments will improve governance of transport service operators, while the police and transport ministry said that Didi holds “unshirkable responsibility” for the rape and murder of a passenger by one of its carpool drivers last week. Didi suspended its Hitch carpooling service at the weekend in response to the incident, while Alibaba-affiliated AutoNavi has also halted its carpool offering. (Reuters)

See: Previous Asia tech news
```