from pyquery import PyQuery as pq
import pickle

def parseDoc(html):
    print(html)
    output = []

    d = pq(html)

    children = d('p, li')
    for c in children:
        output.append(pq(c).text())

    output = list(filter(lambda x: len(x) > 0,output))
    return output

def parsePickle():
    pickleFile = open("inputstrings.pkl", "rb")
    docs =  pickle.load(pickleFile)
    for doc in docs:
        paragraphs = parseDoc(doc)
        for par in paragraphs:
            print(par+"\n")
        print("\n\n###\n\n")

if __name__ == "__main__":
    parsePickle()